package io.catalyte.training.sportsproducts.data;

import io.catalyte.training.sportsproducts.domains.Encounter.Encounter;
import io.catalyte.training.sportsproducts.domains.product.Product;
import io.catalyte.training.sportsproducts.domains.product.ProductRepository;
import io.catalyte.training.sportsproducts.domains.Encounter.EncounterRepository;
import io.catalyte.training.sportsproducts.domains.Patient.Patient;
import io.catalyte.training.sportsproducts.domains.Patient.PatientRepository;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Because this class implements CommandLineRunner, the run method is executed as soon as the server
 * successfully starts, and before it begins accepting requests from the outside.
 * <p>
 * Here, we use this as a place to run some code that generates and saves a list of random
 * products, users, and reviews into the database.
 */
@Component
public class DemoData implements CommandLineRunner {

  private final Logger logger = LogManager.getLogger(DemoData.class);
  ProductFactory productFactory = new ProductFactory();
  UserFactory userFactory = new UserFactory();
  ReviewFactory reviewFactory = new ReviewFactory();
  @Autowired
  private ProductRepository productRepository;
  @Autowired
  private PatientRepository patientRepository;
  @Autowired
  private EncounterRepository encounterRepository;
  @Autowired
  private Environment env;

  @Override
  public void run(String... strings) {
    // Retrieve the value of custom property in application.yml
    boolean loadData = Boolean.parseBoolean(env.getProperty("products.load"));
    if(!loadData) {
      loadData = Boolean.parseBoolean(env.getProperty("users.load"));
    }
    if(!loadData) {
      loadData = Boolean.parseBoolean(env.getProperty("reviews.load"));
    }

    if (loadData) {
      seedDatabase();
    }
  }

  private void seedDatabase() {
    int numberOfProducts;
    int numberOfUsers;
    int maxReviewsPerProduct;

    try {
      // Retrieve the value of custom property in application.yml
      numberOfProducts = Integer.parseInt(env.getProperty("products.number"));
    } catch (NumberFormatException nfe) {
      // If it's not a string, set it to be a default value
      numberOfProducts = 500;
    }

    try {
      // Retrieve the value of custom property in application.yml
      numberOfUsers = Integer.parseInt(env.getProperty("users.number"));
    } catch (NumberFormatException nfe) {
      // If it's not a string, set it to be a default value
      numberOfUsers = 50;
    }

    try {
      // Retrieve the value of custom property in application.yml
      maxReviewsPerProduct = Integer.parseInt(env.getProperty("reviews.maxPerProduct"));
    } catch (NumberFormatException nfe) {
      // If it's not a string, set it to be a default value
      maxReviewsPerProduct = 2;
    }

    // Generate products
    List<Product> productList = productFactory.generateRandomProducts(numberOfProducts);

    // Generate users
    List<Patient> patientList = userFactory.generateRandomUsers(numberOfUsers);

    // Persist them to the database
    logger.info("Loading " + numberOfProducts + " products...");
    productRepository.saveAll(productList);
    logger.info("Loading " + numberOfUsers + " users...");
    patientRepository.saveAll(patientList);

    logger.info("Loading reviews for products...");

    //Generate reviews for the products in the repository
    List<Encounter> allProductsEncounterList = new ArrayList<>();
    for (Product product : productList) {
      List<Encounter> productEncounterList = reviewFactory.generateRandomReviews(product,
          maxReviewsPerProduct, numberOfUsers);
      allProductsEncounterList.addAll(productEncounterList);
    }

    // Persist them to the database
    encounterRepository.saveAll(allProductsEncounterList);
    logger.info("Data load completed.");
  }

}

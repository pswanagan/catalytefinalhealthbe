package io.catalyte.training.sportsproducts.domains.Encounter;

import io.catalyte.training.sportsproducts.domains.product.ProductRepository;
import io.catalyte.training.sportsproducts.domains.Patient.PatientRepository;
import io.catalyte.training.sportsproducts.exceptions.BadRequest;
import io.catalyte.training.sportsproducts.exceptions.ResourceNotFound;
import io.catalyte.training.sportsproducts.exceptions.ServerError;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

/**
 * This class provides the implementation for the EncounterService interface.
 */
@Service
public class EncounterServiceImpl implements EncounterService {

  private final Logger logger = LogManager.getLogger(EncounterServiceImpl.class);

  private final ProductRepository productRepository;
  private final EncounterRepository encounterRepository;
  private final PatientRepository patientRepository;

  @Autowired
  public EncounterServiceImpl(
      ProductRepository productRepository,
      EncounterRepository encounterRepository,
      PatientRepository patientRepository) {
    this.productRepository = productRepository;
    this.encounterRepository = encounterRepository;
    this.patientRepository = patientRepository;
  }


  /**
   * Retrieves all reviews associated with a specific Patient ID.
   *
   * @param id Patient ID used to query the database
   * @return a list of all reviews for a given Patient
   */
  public List<Encounter> getReviewsByUserId(Long id) {
    if (id == null || id.toString().trim().isEmpty()) {
      throw new ResourceNotFound("No Patient ID specified for request.");
    }

    try {
      if (patientRepository.getUserById(id) != null) {
        return encounterRepository.getReviewsByUserId(id);
      } else {
        logger.info("User with ID: " + id + ", does not exist.");
        throw new ResourceNotFound("User with ID: " + id + ", does not exist.");
      }
    } catch (DataAccessException e) {
      logger.error(e.getMessage());
      throw new ServerError(e.getMessage());
    }
  }

  /**
   * Persists a Encounter to the database
   *
   * @param encounter the Encounter to be persisted
   * @return the persisted Encounter with its IDs
   */
  public Encounter saveReview(Encounter encounter) {
    EncounterValidator validator = new EncounterValidator(productRepository, encounterRepository,
            patientRepository);
    try {
      validator.validateReview(encounter);
    } catch (IllegalArgumentException e) {
      logger.error(e.getMessage());
      throw new BadRequest(e.getMessage());
    }

    try {
      encounterRepository.save(encounter);
    } catch (DataAccessException e) {
      logger.error(e.getMessage());
      throw new ServerError(e.getMessage());
    }

    return encounter;
  }
}
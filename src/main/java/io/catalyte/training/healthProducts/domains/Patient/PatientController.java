package io.catalyte.training.sportsproducts.domains.Patient;

import static io.catalyte.training.sportsproducts.constants.Paths.USERS_PATH;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * The PatientController exposes endpoints for Patient related actions.
 */
@RestController
@RequestMapping(value = USERS_PATH)
public class PatientController {

  Logger logger = LogManager.getLogger(PatientController.class);
  @Autowired
  private PatientServiceImpl userServiceImpl;

  public PatientController(PatientServiceImpl userServiceImpl) {
    this.userServiceImpl = userServiceImpl;
  }

  // METHODS

  /**
   * Controller method for logging the Patient in
   *
   * @param patient        User to login
   * @param bearerToken String value in the Authorization property of the header
   * @return User
   */
  @PostMapping(path = "/login")
  public ResponseEntity<Patient> loginUser(
      @RequestBody Patient patient,
      @RequestHeader("Authorization") String bearerToken
  ) {
    logger.info("Request received for User Login");
    return new ResponseEntity<>(userServiceImpl.loginUser(bearerToken, patient), HttpStatus.CREATED);
  }

  /**
   * Controller method for updating the Patient
   *
   * @param id          Id of the Patient to update
   * @param patient        User to update
   * @param bearerToken String value in the Authorization property of the header
   * @return User - Updated Patient
   */
  @PutMapping(path = "/{id}")
  public ResponseEntity<Patient> updateUser(
      @PathVariable Long id,
      @RequestBody Patient patient,
      @RequestHeader("Authorization") String bearerToken
  ) {
    logger.info("Request received for Update User");
    return new ResponseEntity<>(userServiceImpl.updateUser(bearerToken, id, patient), HttpStatus.OK);
  }

  /**
   * It updates the last active timestamp for the Patient on the Patient database.
   * @param patient - Patient object
   * @return new timeStamp
   */
  @PutMapping(path = "/update")
  public ResponseEntity<Patient> updateUserTimestamp(@RequestBody Patient patient){

    return new ResponseEntity<>(userServiceImpl.changePatientTimestamp(patient), HttpStatus.OK);
  }

  @PostMapping
  @ResponseStatus(value = HttpStatus.CREATED)
  public ResponseEntity<Patient> addUserByEmail(@Valid @RequestBody Patient patient) {
    logger.info("Request received for addUserByEmail: " + patient.getEmail());
    return new ResponseEntity<>(userServiceImpl.addUserByEmail(patient), HttpStatus.CREATED);
  }

  @GetMapping(value = "/id/{id}")
  @ResponseStatus(value = HttpStatus.OK)
  public ResponseEntity<Patient> getUserById(@PathVariable Long id) {
    logger.info("Request received for getUsersById: " + id);
    return new ResponseEntity<>(userServiceImpl.getPatientById(id), HttpStatus.OK);
  }


  @GetMapping
  @ResponseStatus(value = HttpStatus.OK)
  public ResponseEntity<Patient> findUserByEmail(@RequestParam(required=false) String email) {
    logger.info( "Request received for findUserByEmail: " + email);
    return new ResponseEntity<>(userServiceImpl.findPatientByEmail(email), HttpStatus.OK);
  }


  /*
  This method handles validation and exception, making sure custom error message will display
   */
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {

    Map<String, String> errors = new HashMap<>();
    ex.getBindingResult().getAllErrors().forEach((error) -> {
      String fieldName = ((FieldError) error).getField();
      String errorMessage = error.getDefaultMessage();
      logger.error("errorMessage {}", errorMessage);
      errors.put(fieldName, errorMessage);
    });
    return errors;
  }


}


package io.catalyte.training.sportsproducts.domains.Patient;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.Optional;


@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {

  Optional<Patient> findUserByEmail(String email);

  Patient findByEmail(String email);

  Patient getUserById(Long id);

}

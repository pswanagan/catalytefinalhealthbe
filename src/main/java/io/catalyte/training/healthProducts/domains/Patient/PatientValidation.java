package io.catalyte.training.sportsproducts.domains.Patient;

import java.util.ArrayList;
import java.util.regex.Pattern;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class PatientValidation {

  /**
   * Validates a Patient's first name field.
   *
   * @param errors An ArrayList of Strings containing the error messages.
   * @param patient User being validated
   */
  public void validateFirstName(ArrayList<String> errors, Patient patient) {
    String firstName = patient.getFirstName();
    validateRequired(errors, firstName, "First name");
    validateAlphabetic(errors, firstName, "First name");
  }

  /**
   * Validates a Patient's first name field.
   *
   * @param errors An ArrayList of Strings containing the error messages.
   * @param patient User being validated
   */
  public void validateLastName(ArrayList<String> errors, Patient patient) {
    String lastName = patient.getLastName();
    validateRequired(errors, lastName, "First name");
    validateAlphabetic(errors, lastName, "First name");
  }

  /**
   * This function validates a Patient's email field.
   *
   * @param errors An ArrayList of Strings containing the error messages.
   * @param patient User being validated
   */
  public void validateEmail(ArrayList<String> errors, Patient patient) {
    String email = patient.getEmail();
    validateRequired(errors, email, "Email");
//    if (email == null || !Pattern.matches("^^.+@.+\\\\..+$", email)) {
    if (email == null || !Pattern.matches("^(.+)@(\\S+)$", email)) {

      errors.add("Invalid email. Email must be in either x@x.x or x@x format.");

    }
  }

  /**
   * Validates a Patient's street address field.
   *
   * @param errors An ArrayList of Strings containing the error messages.
   * @param patient User being validated
   */
  public void validateStreetAddress(ArrayList<String> errors, Patient patient) {
    String streetAddress = patient.getStreetAddress();
    validateRequired(errors, streetAddress, "Street address");
  }

  /**
   * Validates a Patient's street address field.
   *
   * @param errors An ArrayList of Strings containing the error messages.
   * @param patient User being validated
   */
  public void validateCity(ArrayList<String> errors, Patient patient) {
    String city = patient.getCity();
    validateRequired(errors, city, "City");
    validateAlphabetic(errors, city, "City");
  }

  /**
   * Validates a Patient's state field
   *
   * @param errors An ArrayList of Strings containing the error messages.
   * @param patient User being validated
   */
  public void validateState(ArrayList<String> errors, Patient patient) {
    String state = patient.getState();
    validateRequired(errors, state, "State");
    if (state == null || !Pattern.matches(
        "^(?-i:A[LKSZRAEP]|C[AOT]|D[EC]|F[LM]|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEHINOPST]|N[CDEHJMVY]|O[HKR]|P[ARW]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY])$",
        state)) {
      errors.add("Invalid State. Please enter a valid two-letter US state abbreviation.");
    }
  }

  /**
   * Validates a Patient's zip code field
   *
   * @param errors An ArrayList of Strings containing the error messages.
   * @param patient User being validated
   */
  public void validateZipCode(ArrayList<String> errors, Patient patient) {
    String zipCode = patient.getPostal();
    validateRequired(errors, zipCode, "Zip code");
    if (zipCode == null || !Pattern.matches("^[0-9]{5}(?:-[0-9]{4})?$", zipCode)) {
      errors.add(
          "Invalid Zip Code. Zip Code must be in either the xxxxx or xxxxx-xxxx format, where 'x' represents a digit.");
    }
  }


  /**
   * Validates required Patient fields.
   *
   * @param errors An ArrayList of Strings containing the error messages.
   * @param requiredInputToValidate String that will be validated
   * @param inputNameForErrorMessage Name of the input to be used for the dynamic error message.
   */
  public void validateRequired(ArrayList<String> errors, String requiredInputToValidate,
      String inputNameForErrorMessage) {
    if (requiredInputToValidate == null || requiredInputToValidate.trim().equals("")) {
      errors.add(String.format("%s; may not be blank, empty or null", inputNameForErrorMessage));
    }
  }

  /**
   * Validates alphabetic Patient fields.
   *
   * @param errors An ArrayList of Strings containing the error messages.
   * @param requiredInputToValidate String that will be validated
   * @param inputNameForErrorMessage Name of the input to be used for the dynamic error message.
   */
  public void validateAlphabetic(ArrayList<String> errors, String requiredInputToValidate,
      String inputNameForErrorMessage) {
    if (requiredInputToValidate == null || !Pattern.matches("^([A-Za-z])[A-Za-z '-]*$", requiredInputToValidate)) {
      errors.add(String
          .format("Invalid Input. %s; may only allow letters, apostrophes, spaces, hyphens (-)",
              inputNameForErrorMessage));
    }
  }

  /**
   * Validates all Patient fields
   *
   * @param patient User being validated
   */
  public void validateUser(Patient patient) {
    ArrayList<String> errors = new ArrayList<>();
    if (patient == null) {
      errors.add("No Patient provided");
      processBadUserValidation(errors);
    }

    validateFirstName(errors, patient);
    validateLastName(errors, patient);
    validateEmail(errors, patient);
    validateStreetAddress(errors, patient);
    validateCity(errors, patient);
    validateState(errors, patient);
    validateZipCode(errors, patient);

    if (!errors.isEmpty()) {
      processBadUserValidation(errors);
    }
  }

  /**
   * Validation helper method to throw exception for creating Patient with appropriate message
   *
   * @param errors ArrayList of error messages detailing what caused validation to fail
   */
  public void processBadUserValidation(ArrayList<String> errors) {
    StringBuilder message = new StringBuilder();
    for (int i = 0; i < errors.size(); i++) {
      message.append(errors.get(i));
      if (i < errors.size() - 1) {
        message.append("; ");
      }
    }
    throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
        "User could not be created - " + message);
  }

}

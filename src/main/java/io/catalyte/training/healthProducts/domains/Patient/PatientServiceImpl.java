package io.catalyte.training.sportsproducts.domains.Patient;

import io.catalyte.training.sportsproducts.auth.GoogleAuthService;
import io.catalyte.training.sportsproducts.exceptions.ResourceNotFound;
import io.catalyte.training.sportsproducts.exceptions.ServerError;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


/**
 * This class provides the implementation for the PatientService interface. =======
 */

@Service
public class PatientServiceImpl implements PatientService {

  private final Logger logger = LogManager.getLogger(PatientServiceImpl.class);

  @Autowired
  private final PatientRepository patientRepository;
  private final GoogleAuthService googleAuthService = new GoogleAuthService();
  private final PatientValidation patientValidation = new PatientValidation();

  public PatientServiceImpl(PatientRepository patientRepository) {
    this.patientRepository = patientRepository;
  }

  /**
   * Retrieves the Patient with the provided id from the database.git
   *
   * @param id - the id of the Patient to retrieve
   * @return - the Patient
   */
  public Patient getPatientById(Long id) {
    Patient patient;
    try {
      patient = patientRepository.findById(id).orElse(null);
    } catch (DataAccessException e) {
      logger.error(e.getMessage());
      throw new ServerError(e.getMessage());
    }
    if (patient != null) {
      return patient;
    } else {
      logger.info("Get by id failed, id does not exist in the database: " + id);
      throw new ResourceNotFound(
          "Get by id failed. id " + id + " does not exist in the database: ");
    }
  }


  /**
   * Retrieves the Patient info with the matching email from the database.
   * @param email - Patient email to find
   * @return Patient
   */
  public Patient findPatientByEmail(String email) {
    Patient patient;
    try {
      patient = patientRepository.findByEmail(email);
    } catch (DataAccessException e) {
      logger.error(e.getMessage());
      throw new ServerError(e.getMessage());
    }
    if (patient != null) {
      return patient;
    } else {
      logger.info("Get by email failed, email does not exist in the database" + email);
      throw new ResourceNotFound(
          "Get by email failed. email " + email + "does not exist in the database: "
      );
    }
  }

  /**
   * Adds a new Patient with unique email to the database.
   *
   * @param patient - the Patient
   * @return - the Patient
   */
  public Patient addUserByEmail(Patient patient) {
    patientValidation.validateUser(patient);
    Optional<Patient> userOptional = patientRepository.findUserByEmail(patient.getEmail());
    if (userOptional.isPresent()) {
      logger.info("Add new Patient failed, email already exists in the database: " + patient.getEmail());
      throw new IllegalStateException("Email already exists: " + patient.getEmail());
    }
    patientRepository.save(patient);
    System.out.println(patient);
    return patient;
  }

  /**
   * Updates a User given they are given the right credentials
   *
   * @param bearerToken - String value in the Authorization property of the header
   * @param id          - id of the Patient to update
   * @param updatedPatient - User to update
   * @return User - Updated Patient
   */
  @Override
  public Patient updateUser(String bearerToken, Long id, Patient updatedPatient) {

    // AUTHENTICATES USER - SAME EMAIL, SAME PERSON
    String token = googleAuthService.getTokenFromHeader(bearerToken);
    boolean isAuthenticated = googleAuthService.authenticateUser(token, updatedPatient);

    if (!isAuthenticated) {
      logger.error("Email in the request body does not match email from JWT");
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
          "Email in the request body does not match email from JWT Token");
    }

    // UPDATES USER
    Patient existingPatient;

    try {
      existingPatient = patientRepository.findById(id).orElse(null);
    } catch (DataAccessException dae) {
      logger.error(dae.getMessage());
      throw new ServerError(dae.getMessage());
    }

    if (existingPatient == null) {
      logger.error("User with id: " + id + " does not exist");
      throw new ResourceNotFound("User with id: " + id + " does not exist");
    }



    // GIVE THE USER ID IF NOT SPECIFIED IN BODY TO AVOID DUPLICATE USERS
    if (updatedPatient.getId() == null) {
      updatedPatient.setId(id);
    }

    try {
      logger.info("Saved Patient to");
      return patientRepository.save(updatedPatient);
    } catch (DataAccessException dae) {
      logger.error(dae.getMessage());
      throw new ServerError(dae.getMessage());
    }

  }

  /**
   * Logs Patient in with Google Auth - Returns existing Patient information or creates a new Patient
   *
   * @param bearerToken String value in the Authorization property of the header
   * @param patient        User to login
   * @return User
   */
  @Override
  public Patient loginUser(String bearerToken, Patient patient) {

    // AUTHENTICATES USER
    // Authenticating the Patient ensures that the Patient is using Google to sign in
    String token = googleAuthService.getTokenFromHeader(bearerToken);
    boolean isAuthenticated = googleAuthService.authenticateUser(token, patient);

    if (!isAuthenticated) {
      logger.error("Email in the request body does not match email from JWT");
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
              "Email in the request body does not match email from JWT Token");
    }

    // SEE IF Patient EXISTS
    Patient existingPatient;

    //create a clock
    ZoneId zid = ZoneId.of("UTC");
    // create an LocalDateTime object using now(zoneId)
    LocalDateTime lt = LocalDateTime.now(zid);
    try {
      existingPatient = patientRepository.findByEmail(patient.getEmail());
    } catch (DataAccessException dae) {
      logger.info(dae.getMessage());
      throw new ServerError(dae.getMessage());
    }

    // IF Patient EXISTS, RETURN EXISTING Patient WITH UPDATED TIMESTAMP
    if (existingPatient != null) {
      logger.info("Existing Patient has been found");
      existingPatient.setLastActiveTime(lt);
      patientRepository.save(existingPatient);
      return existingPatient;
    }

    // ELSE CREATE NEW USER WITHOUT VALIDATION
    patient.setLastActiveTime(lt);
    return createUserWithoutValidation(patient);
  }

  /**
   * updates Patient timestamp
   * @param patient - Patient object
   * @return updated timestamp
   */
  @Override
  public Patient changePatientTimestamp(Patient patient){

    // SEE IF USER EXISTS
    Patient existingPatient;

    //create a clock
    ZoneId zid = ZoneId.of("UTC");
    // create an LocalDateTime object using now(zoneId)
    LocalDateTime lt = LocalDateTime.now(zid);
    try {
      existingPatient = patientRepository.findByEmail(patient.getEmail());
    } catch (DataAccessException dae) {
      logger.info(dae.getMessage());
      throw new ServerError(dae.getMessage());
    }

    // IF Patient EXISTS, RETURN EXISTING Patient WITH UPDATED TIMESTAMP
    if (existingPatient != null) {
      logger.info("Existing Patient has been found");
      existingPatient.setLastActiveTime(lt);
      logger.info("User timestamp has been updated");
      patientRepository.save(existingPatient);
      return existingPatient;
    }

    // ELSE CREATE NEW USER WITHOUT VALIDATION
    patient.setLastActiveTime(lt);
    logger.info("User timestamp has been updated");
    return createUserWithoutValidation(patient);


  }



  /**
   * Creates a Patient in the database, given email is not null and not taken
   *
   * @param patient User to create
   * @return User
   */
  @Override
  public Patient createPatient(Patient patient) {

    String email = patient.getEmail();

    // CHECK TO MAKE SURE EMAIL EXISTS ON INCOMING USER
    if (email == null) {
      logger.error("User must have an email field");
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User must have an email field");
    }

    // VALIDATE THE Patient
    patientValidation.validateUser(patient);

    // CHECK TO MAKE SURE Patient EMAIL IS NOT TAKEN
    Patient existingPatient;

    try {
      existingPatient = patientRepository.findByEmail(patient.getEmail());
    } catch (DataAccessException dae) {
      logger.error(dae.getMessage());
      throw new ServerError(dae.getMessage());
    }

    if (existingPatient != null) {
      logger.error("Email is taken");
      throw new ResponseStatusException(HttpStatus.CONFLICT, "Email is taken");
    }



    // SAVE Patient
    try {
      logger.info("Saving Patient");
      return patientRepository.save(patient);
    } catch (DataAccessException dae) {
      logger.error(dae.getMessage());
      throw new ServerError(dae.getMessage());
    }

  }

  /**
   * Creates a Patient without Validation
   *
   * @param patient User to create
   * @return Saved User
   */
  @Override
  public Patient createUserWithoutValidation(Patient patient) {
    String email = patient.getEmail();

    // CHECK TO MAKE SURE EMAIL EXISTS ON INCOMING USER
    if (email == null) {
      logger.error("User must have an email field");
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User must have an email field");
    }

    // CHECK TO MAKE SURE USER EMAIL IS NOT TAKEN
    Patient existingPatient;

    try {
      existingPatient = patientRepository.findByEmail(patient.getEmail());
    } catch (DataAccessException dae) {
      logger.error(dae.getMessage());
      throw new ServerError(dae.getMessage());
    }

    if (existingPatient != null) {
      logger.error("Email is taken");
      throw new ResponseStatusException(HttpStatus.CONFLICT, "Email is taken");
    }


    // SAVE Patient
    try {
      logger.info("Saving Patient");
      return patientRepository.save(patient);
    } catch (DataAccessException dae) {
      logger.error(dae.getMessage());
      throw new ServerError(dae.getMessage());
    }

  }


}

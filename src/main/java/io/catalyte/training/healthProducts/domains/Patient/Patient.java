package io.catalyte.training.sportsproducts.domains.Patient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.catalyte.training.sportsproducts.domains.Encounter.Encounter;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * This class is a representation of a Patient.
 */
@Entity
@Table(name = "Patient", schema = "public")
@JsonInclude(Include.NON_NULL)
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String ssn;
    private Long age;
    private Long height;
    private Long weight;
    private String insurance;
    private String gender;


    private String streetAddress;
    private String streetAddress2;
    private String city;
    private String state;
    private String postal;

    private String email;

    @Column(name = "last_active_time", columnDefinition = "timestamp with time zone")
    private LocalDateTime lastActiveTime;

    @OneToMany(mappedBy = "userId")
    private List<Encounter> encounters;

    public Patient() {
    }

    public Patient(String firstName, String lastName, String ssn, String insurance, String gender, Long age, Long height, Long weight, String email, String streetAddress,
                   String streetAddress2, String city, String state, String postal,
                   LocalDateTime lastActiveTime,
                   List<Encounter> encounters) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.ssn = ssn;
        this.insurance = insurance;
        this.gender = gender;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.email = email;
        this.streetAddress = streetAddress;
        this.streetAddress2 = streetAddress2;
        this.city = city;
        this.state = state;
        this.postal = postal;
        this.lastActiveTime = lastActiveTime;
        this.encounters = encounters;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public Long getWeight() {
        return weight;
    }

    public void setWeight(Long weight) {
        this.weight = weight;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getStreetAddress2() {
        return streetAddress2;
    }

    public void setStreetAddress2(String StreetAddress2) {
        this.streetAddress2 = StreetAddress2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }


    public List<Encounter> getReviews() {
        return encounters;
    }

    public void setReviews(List<Encounter> encounters) {
        this.encounters = encounters;
    }

    public LocalDateTime getLastActiveTime() {
        return lastActiveTime;
    }

    public void setLastActiveTime(LocalDateTime lastActiveTime) {
        this.lastActiveTime = lastActiveTime;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", ssn='" + ssn +'\'' +
                ", gender='" + gender + '\'' +
                ", age='" + age + '\'' +
                ", height='" + height + '\'' +
                ", weight='" + weight + '\'' +
                ", streetAddress='" + streetAddress + '\'' +
                ", streetAddress2='" + streetAddress2 + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", postal='" + postal + '\'' +
                ", lastActiveTime=" + lastActiveTime +
                ", encounters=" + encounters +
                '}';
    }
}




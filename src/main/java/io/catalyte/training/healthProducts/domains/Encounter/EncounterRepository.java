package io.catalyte.training.sportsproducts.domains.Encounter;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EncounterRepository
    extends JpaRepository<Encounter, Long> {


  List<Encounter> getReviewsByUserId(Long id);

}

package io.catalyte.training.sportsproducts.domains.Encounter;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.apache.logging.log4j.core.config.plugins.validation.constraints.Required;

/**
 * This class represents a sports apparel product Encounter.
 */
@Entity
public class Encounter {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Required
  private Long userId;


  @Required
  private Integer rating;

  private String title;

  @Column(columnDefinition = "TEXT")
  private String comment;

  @Required
  private LocalDate date;

  public Encounter() {
  }

  public Encounter(
      Long id,
      Long userId,
      Integer rating,
      String title,
      String comment,
      LocalDate date) {
    this.id = id;
    this.userId = userId;
    this.rating = rating;
    this.title = title;
    this.comment = comment;
    this.date = date;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }


  public Integer getRating() {
    return rating;
  }

  public void setRating(Integer rating) {
    this.rating = rating;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  @Override
  public String toString() {
    return "Encounter{" +
        "id=" + id +
        ", userId='" + userId + '\'' +
        ", rating='" + rating + '\'' +
        ", title='" + title + '\'' +
        ", comment='" + comment + '\'' +
        ", date='" + date + '\'' +
        '}';
  }
}

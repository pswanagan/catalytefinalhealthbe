package io.catalyte.training.sportsproducts.domains.Encounter;

import io.catalyte.training.sportsproducts.domains.product.ProductRepository;
import io.catalyte.training.sportsproducts.domains.product.ProductServiceImpl;
import io.catalyte.training.sportsproducts.domains.Patient.PatientRepository;
import java.time.LocalDate;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class EncounterValidator {

  private final Logger logger = LogManager.getLogger(EncounterValidator.class);

  private final ProductRepository productRepository;
  private final EncounterRepository encounterRepository;
  private final PatientRepository patientRepository;

  @Autowired
  public EncounterValidator(
      ProductRepository productRepository,
      EncounterRepository encounterRepository,
      PatientRepository patientRepository) {
    this.productRepository = productRepository;
    this.encounterRepository = encounterRepository;
    this.patientRepository = patientRepository;
  }

  /**
   * Validates Encounter fields before it is saved
   *
   * @param encounter the Encounter to validate
   */
  void validateReview(Encounter encounter) {
    ArrayList<String> errors = new ArrayList<>();

    if (encounter == null) {
      errors.add("No Encounter provided");
      rejectReview(errors);
    }

    assert encounter != null;
    validateUserId(errors, encounter);

    validateRating(errors, encounter);

    validateTitle(errors, encounter);

    if (!errors.isEmpty()) {
      rejectReview(errors);
    }
  }

  /**
   * Validates the Patient ID is provided and that it exists in the database.
   *
   * @param errors running list of errors
   * @param encounter Encounter being verified
   */
  private void validateUserId(ArrayList<String> errors, Encounter encounter) {
    try {
      Long userId = encounter.getUserId();
      if (userId < 1 || userId > patientRepository.count()) {
        errors.add("User not found in database");
      }
    } catch (NullPointerException e) {
      errors.add("Encounter must have a Patient ID associated with it");
    }
  }


  /**
   * Validates the rating is provided and that it is a positive integer between 1 and 5.
   *
   * @param errors running list of errors
   * @param encounter Encounter being verified
   */
  private void validateRating(ArrayList<String> errors, Encounter encounter) {
    Integer rating = encounter.getRating();

    if (rating < 1 || rating > 5) {
      errors.add("Product rating must be a positive integer 1 - 5");
    }
  }

  /**
   * Validates the Encounter has a date, and that the date is between the product release and today's
   * date. This method requires the Encounter has valid product ID associated with it.
   *
   * @param errors running list of errors
   * @param encounter Encounter being verified
   */
  private void validateReviewDate(ArrayList<String> errors, Encounter encounter) {
    try {
      LocalDate reviewDate = encounter.getDate();
      LocalDate productReleaseDate;

      if (reviewDate == null) {
        errors.add("Encounter must have a Encounter date");
        return;
      }



    } catch (IllegalArgumentException e) {
      logger.error(e.getMessage());
    }
  }

  /**
   * Validates the title length does not exceed the 255-character limit.
   *
   * @param errors running list of errors
   * @param encounter Encounter being verified
   */
  private void validateTitle(ArrayList<String> errors, Encounter encounter) {
    int titleLength = encounter.getTitle().length();

    if (titleLength > 255) {
      errors.add("Encounter title character length exceeded (max 255 characters)");
    }
  }

  /**
   * Validation helper method to throw exception with appropriate message
   *
   * @param errors - running list of error messages detailing what caused validation to fail
   */
  private void rejectReview(ArrayList<String> errors) {
    StringBuilder message = new StringBuilder();
    for (int i = 0; i < errors.size(); i++) {
      message.append(errors.get(i));
      if (i < errors.size() - 1) {
        message.append("; ");
      }
    }
    throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
        "The Encounter was unable to be saved - " + message);
  }

}

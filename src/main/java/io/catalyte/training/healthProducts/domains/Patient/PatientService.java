package io.catalyte.training.sportsproducts.domains.Patient;

/**
 * This interface provides an abstraction layer for the User Service
 */
public interface PatientService {

  Patient updateUser(String credentials, Long id, Patient patient);

  Patient loginUser(String credentials, Patient patient);

  Patient createPatient(Patient patient);

  Patient changePatientTimestamp(Patient patient);

  Patient getPatientById(Long id);

  Patient findPatientByEmail(String email);

  Patient createUserWithoutValidation(Patient patient);


}

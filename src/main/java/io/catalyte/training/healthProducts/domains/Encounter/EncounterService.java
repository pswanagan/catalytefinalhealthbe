package io.catalyte.training.sportsproducts.domains.Encounter;

import java.util.List;
import org.springframework.stereotype.Service;

/**
 * This interface provides an abstraction layer for the Reviews Service
 */
@Service
public interface EncounterService {


  List<Encounter> getReviewsByUserId(Long id);

  Encounter saveReview(Encounter encounter);
}

package io.catalyte.training.sportsproducts.domains.Encounter;

import static io.catalyte.training.sportsproducts.constants.Paths.REVIEWS_PATH;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Exposes endpoints for the Encounter domain
 */
@RestController
@RequestMapping(value = REVIEWS_PATH)
public class EncounterController {

  Logger logger = LogManager.getLogger(EncounterController.class);

  @Autowired
  private EncounterService encounterService;


  @GetMapping(value = "/Patient")
  @ResponseStatus(value = HttpStatus.OK)
  public ResponseEntity<List<Encounter>> getReviewsByUserId(@RequestParam(required = false) Long id) {
    logger.info("Request received to get reviews from Patient id: " + id);

    return new ResponseEntity<>(encounterService.getReviewsByUserId(id), HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity saveReview(@RequestBody Encounter encounter) {

    encounterService.saveReview(encounter);

    return new ResponseEntity<>(encounter, HttpStatus.CREATED);
  }
}

package io.catalyte.training.sportsproducts.domains.Patient;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PatientInputValidationTest {
  private static ValidatorFactory validatorFactory;
  private static Validator validator;

  @BeforeClass
  public static void createValidator(){
    validatorFactory = Validation.buildDefaultValidatorFactory();
    validator = validatorFactory.getValidator();
  }

  @AfterClass
  public static void close(){
    validatorFactory.close();
  }

  @Test
  public void shouldHaveNoViolations(){
    Patient patient = new Patient();
    patient.setFirstName("John");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertTrue(violations.isEmpty());
  }

  @Test
  public void firstNameIsNull(){
    Patient patient = new Patient();
    patient.setFirstName(null);
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void firstNameIsEmpty(){
    Patient patient = new Patient();
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void firstNameIsBlank(){
    Patient patient = new Patient();
    patient.setFirstName("");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }
  @Test
  public void firstNameContainsOnlyNumbers(){
    Patient patient = new Patient();
    patient.setFirstName("33");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void firstNameContainsNotAllowedSpecialCharacters(){
    Patient patient = new Patient();
    patient.setFirstName("#@");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void firstNameContainsOnlyAllowedSpecialCharacters(){
    Patient patient = new Patient();
    patient.setFirstName("'- ");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void firstNameContainsNumbersAndAllowedSpecialCharacters(){
    Patient patient = new Patient();
    patient.setFirstName("34'- ");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }
  @Test
  public void firstNameContainsLettersAndAllowedAndNotAllowedSpecialCharacters(){
    Patient patient = new Patient();
    patient.setFirstName("ee@- ");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }
  @Test
  public void firstNameContainsLettersAndSpaces(){
    Patient patient = new Patient();
    patient.setFirstName("dg ");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), true);
  }
  @Test
  public void firstNameContainsLettersAndHyphens(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), true);
  }


  @Test
  public void streetAddressIsEmpty(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void streetAddress2IsEmpty(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");

    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), true);
  }

  @Test
  public void lastNameIsNull(){
    Patient patient = new Patient();
    patient.setFirstName("john");
    patient.setLastName(null);
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void lastNameIsEmpty(){
    Patient patient = new Patient();
    patient.setFirstName("john");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void lastNameIsBlank(){
    Patient patient = new Patient();
    patient.setFirstName("john");
    patient.setLastName("");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }
  @Test
  public void lastNameContainsOnlyNumbers(){
    Patient patient = new Patient();
    patient.setFirstName("john");
    patient.setLastName("33");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void lastNameContainsNotAllowedSpecialCharacters(){
    Patient patient = new Patient();
    patient.setFirstName("john");
    patient.setLastName("#$");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void lastNameContainsOnlyAllowedSpecialCharacters(){
    Patient patient = new Patient();
    patient.setFirstName("john");
    patient.setLastName("'- ");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void lastNameContainsNumbersAndAllowedSpecialCharacters(){
    Patient patient = new Patient();
    patient.setFirstName("john");
    patient.setLastName("34'-");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }
  @Test
  public void lastNameContainsLettersAndAllowedAndNotAllowedSpecialCharacters(){
    Patient patient = new Patient();
    patient.setFirstName("john");
    patient.setLastName("smith@_-");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }
  @Test
  public void lastNameContainsLettersAndSpaces(){
    Patient patient = new Patient();
    patient.setFirstName("john");
    patient.setLastName("smith ");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), true);
  }
  @Test
  public void lastNameContainsLettersAndHyphens(){
    Patient patient = new Patient();
    patient.setFirstName("john");
    patient.setLastName("smith-");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 abc street");
    patient.setStreetAddress2("apt 1");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), true);
  }

  @Test
  public void cityIsEmpty(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void cityContainsNumbers(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New Y33");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void cityContainsNotAllowedSpecialCharacters(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New Y#@");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void cityContainsHyphens(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("Winston-Salem");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), true);
  }

  @Test
  public void cityContainsApostrophes(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("Coeur d'Alene");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), true);
  }

  @Test
  public void cityContainsSpaceAndApostrophes(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("Coeur d'Alene");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), true);
  }
  @Test
  public void cityContainsSpace(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), true);
  }

  @Test
  public void stateIsEmpty(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New York");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void stateIsSmallLetters(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New York");
    patient.setState("ny");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void stateIsMoreLetters(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New York");
    patient.setState("NJH");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void stateIsNotValid(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New York");
    patient.setState("BJ");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void stateIsValid(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12324");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), true);
  }

  @Test
  public void zipCodeIsLetters(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("gujs");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void zipCodeIsSixDigits(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("123456");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void zipCodeIsFiveDigitsPlusOneDigits(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12345-6");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void zipCodeIsValid(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("12345-6789");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), true);
  }

  @Test
  public void zipCodeIsNineDigitsWithNoHyphen(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("123456789");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void phoneNumberHasLetters(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("123456789");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void phoneNumberHasLessNumbers(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("123456789");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void phoneNumberHasSpecialCharacters(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("123456789");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(violations.isEmpty(), false);
  }

  @Test
  public void phoneNumberHasHyphens(){
    Patient patient = new Patient();
    patient.setFirstName("dg-");
    patient.setLastName("smith");
    patient.setEmail("smith@gmail.com");
    patient.setStreetAddress("123 main street");
    patient.setCity("New York");
    patient.setState("NY");
    patient.setPostal("123456789");
    Set<ConstraintViolation<Patient>> violations = validator.validate(patient);
    assertEquals(!violations.isEmpty(), false);
  }


}
